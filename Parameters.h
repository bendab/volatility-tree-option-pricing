#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <cmath>
#include <iostream>

using namespace std;

//BM, RV:=Random Variable
inline double RV_Zeta (double y0, int n_ld, int k);
inline double RV_Xi (double x0, int l, int k);
//Bm, RV:=Random Variable

////Laplace
inline double g(double t);
inline double gn (int k, double *gamma_i, double *c_i, int n_ld);
////Laplace

inline double mu_x(int k, double X, double Y);
inline double sigma_x(int k, double V);
inline double alpha (int k, double V);

inline double mu_y (int k, double Y);
inline double sigma_y (int k, double Y);
inline double beta (int k, double Y);

inline double pi_m(int m);
inline double mu_measure ();
inline double c (double eta_i, double eta_i_minus);
inline double gamma (double ci, double eta_i, double eta_i_minus);

//H==0.1==rough//=little gamma Hurst exponent//0.5=Classic Brownian motion
const double H = 0.5;
const int n = 350;//Time steps
const int n_ld = 1;//Laplace discretization

//table 4
double T = 0.25;//Horizon
double K = 10.0;//Strike
double rate = 0.1;// log (1.0+10.0/100.0);//0.05;0.1f;
double eta = 0.90;
double kappa = 5.0;
double theta = 0.16;
double rho = 0.1;
double nu_0 = 0.25*0.25;
double S_0 = 12.0;//Spot price

////table 3
//double T = 0.50;//Horizon
//double K = 100.0;//Strike
//double rate = 0.05;//log (1.0+5.0/100.0);//0.05;
//double eta = 0.1;//vol of vol
//double kappa = 3.0;//rate of reverting
//double theta = 0.04;//long run variance
//double rho = -0.7;
//double nu_0 = 0.4*0.4;//0.2*0.2;
//double S_0 = 100.0;//Spot price

////Premia
//double T = 0.5;//Horizon
//double K = 10.0;//Strike
//double rate = 0.05;//log (1.0+10.0/100.0);//0.05;0.1f;
//double eta = 0.90;
//double kappa = 5.0;
//double theta = 0.16;
//double rho = 0.1;
//double nu_0 = 0.5*0.5;
//double S_0 = 12.0;//Spot price

const double delta = T/(static_cast<double>(n));//Time steps
const double sqrt_delta = sqrt(delta);

double x_0;// = ln(S_0);
double y_0;// = nu_0;
double v_0;// = nu_0;

//Bound
double X_min = .0;
double X_max = 500.0;

double Y_min = .0;
double Y_max = 500.0;//pow( (kappa*theta/eta + abs(rho)*rate)*sqrt_delta/(sqrt(eta*(1.0-rho*rho))), 2);//0.225;//

//X
inline double alpha (int k, double V)
{
    //To correct because V > 0 at any time i.e. Feller condition
    if(k <= 0)
        return 0.0;
    //    double A = (kappa*theta/eta + abs(rho)*rate)*sqrt_delta/(eta*(1.0-rho*rho));
    double tmp_sigma_x = sigma_x(k, V);

    return 0.5*(max (0.0, tmp_sigma_x*tmp_sigma_x) - 1.0);
}

inline double mu_x(int k, double X, double Y)
{
//    if (k < 0)
//        return 0.0;
    return /*rate*/ - 0.5*Y;
}

inline double sigma_x(int k, double Y)
{
    if (Y < 0.000)
        return 0.0;

    return sqrt(Y);
}
//X

//Volatility
//    double A = (kappa*theta/eta + abs(rho)*rate)*sqrt_delta;
inline double beta (int k, double Y)
{
    if(k <= 0)//GET RID OF =
        return 0.0;
    //    double A = (kappa*theta/eta + abs(rho)*rate)*sqrt_delta/(eta*(1.0-rho*rho));
    double tmp_sigma_y = sigma_y(k, Y);

    return 0.5*(max (0.0, tmp_sigma_y*tmp_sigma_y) - 1.0);
}

//sum_tmp = mean reversion sum term
//g_t = g(t) is deterministic so we compute once at each time step
inline double mu_y (int k, double Y)
{
//    if (k <= 0)
//        return 0.0;
    return kappa*(theta - Y);
}

inline double sigma_y (int k, double Y)
{
    if (Y < 0.000)
        return 0.0;

    return eta * sqrt(Y);
}
//Volatility

//Brownian Motion
inline double RV_Xi (double x0, int l, int k)
{
//    if (k <= 0)
//        return x0;//xi_0 = 0
//        if (k >= 0)
//        {
//            if (l>k)//edge sup
//                l = -10000;
//            else if (l<-k)//edge inf
//                l = -10000;
//        }
    double X_tmp = x0 + sqrt_delta*static_cast<double>(l);
    //    double X_tmp = x0 + sqrt_delta*(2.0*static_cast<double>(l) - (static_cast<double>(k)+1.0));
    return max( X_min, min (X_max, X_tmp));
}

inline double RV_Zeta (double y0, int m, int k)
{
//    if (k <= 0)
//        return y0;
//        if (k >= 0)
//        {
//            if (m>k)//edge sup
//                m = -10000;
//            else if (m<-k)//edge inf
//                m = 10000;
//        }
    double Y_tmp = y0 + sqrt_delta*static_cast<double>(m);
    //    double Y_tmp = y0 + sqrt_delta*(2.0*static_cast<double>(m) - (static_cast<double>(k)+1.0));
    return max( Y_min, min (Y_max, Y_tmp) );
}
//Brownian Motion












/***************ROUGH******************/

/**
 * @brief g continuous version see formula (3.1) in AJEE
 * @param t time : k*delta, k=0,..,n.
 * @return g(t)
 */
inline double g (double t)
{
    double C = 1.0f / tgamma(H+0.5);
    double tmp_Integral = 2.0f*pow(t, H+0.5)/(2.0f*H+1);
    double theta_u = (kappa*theta);

    return v_0 + C * theta_u * tmp_Integral;
}

/**
 * @brief gn discontinuous version see formulas (3.3)-(3.4)
 * @param t : time must be equal to k*delta with k=0,...,n and delta = T/n.
 * @param gamma_i provide by KDM
 * @param c_i provide by KDM
 * @return
 */
inline double gn (int k, double * gamma_i, double * c_i, int n_ld)
{
    double t = k*delta;
    double _sum = 0.0f;//v_0 + kappa*theta*t;

    if (H < 0.5)//Rough
    {
        double I;//integral 0 to t then see 3.4
        for (int i = 0; i < n_ld; i++)
        {
            I = (1.0f/gamma_i[i]) * (1.0f - exp(-gamma_i[i]*t));
            _sum += c_i[i] * I;//CHECK AGAIN
        }
    }
    else if (H >= 0.499)
    {
        _sum = y_0;
    }

    return _sum;
}

inline double delta_gn (int k, double * gamma_i, double * c_i, int n_ld)
{
    if (k > 1)
        return gn (k, gamma_i, c_i, n_ld) - gn (k-1, gamma_i, c_i, n_ld);
    return 0.0f;
}


/**
 * @brief mu_measure
 * @param gamma integral discrete variable
 * @return
 */
inline double mu_measure (double gamma)
{
    return pow(gamma,-H-0.5)/(tgamma(H+0.5)*tgamma(0.5-H));
}

/**
 * @brief c
 * @param eta_i
 * @param eta_i_minus
 * @return return c, see (3.6) in AJEE 2019 :
 * return mu_measure(eta_i)-mu_measure(eta_i_minus)
 */
inline double c (double eta_i, double eta_i_minus)
{
    double tmp1 = pow(eta_i,-H-0.5);
    double tmp2 = pow(eta_i_minus,-H-0.5);

    return (tmp1 - tmp2)/(tgamma(H+0.5)*tgamma(0.5-H));
}

/**
 * @brief gamma
 * @param ci
 * @param eta_i
 * @param eta_i_minus
 * @return
 */
inline double gamma (double ci, double eta_i, double eta_i_minus)
{
    double tmp = (1.0f/ci) * ( 1.0f/(tgamma(H+0.5)*tgamma(0.5-H)*(1.5-H)) );
    return tmp * (pow(eta_i,1.5-H) - pow(eta_i_minus,1.5-H));
}

/**
 * @brief pi_m
 * @param m discretization parameter
 * @return pi for initiate the algo, see eq. 3.10 in AJEE Multifactor ... 2019
 */
inline double pi_m(int m)
{
    double tmp = pow (m, -0.2)/T;
    double tmp2 = (sqrt(10.0f)*(1.0f-2.0f*H))/(5.0f-2.0f*H);
    double pi = tmp * pow (tmp2, 0.4);
    return pi;
}
//pi for initiate the algo


#endif // PARAMETERS_H
