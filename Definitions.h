#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <map>
#include <iostream>
#include <cmath>
#include <vector>
#include <chrono>
#include "fastExponential.h"

using namespace std;
using namespace std::chrono;

// first form
/***Final Value***/
typedef map<double, double> p1d;
typedef map<double, p1d> p2d;
typedef vector<p2d> v3d;
typedef vector<v3d> v4d;

typedef vector<double> vec_1d;
typedef vector<vec_1d> vec_2d;
typedef vector<vec_2d> vec_3d;
typedef vector<vec_3d> vec_4d;
/***Final Value***/

/***For Multifactor Uk***/
typedef vector<p1d> v2d;
typedef vector<double> v1d;//useless
/***For Multifactor Uk***/

/***ROUGH VOLATILITY***/
typedef vector<p1d> v2d;
//typedef vector<v2d> rg_v3d;
typedef vector<v1d> rg_v2d;
/***ROUGH VOLATILITY***/

double **** init_4d_pointer (int size);
void free_4d_pointer (double **** p, int size);

double **** init_4d_pointer (int size)
{
    double **** value_at_n;
    value_at_n = (double ****) malloc( (size + 1) * sizeof (*value_at_n) );
    if (value_at_n == NULL)
        exit (1);
    for (int i = 0; i < size+1; i++)
    {
        value_at_n [i] = (double ***) malloc( (size + 1) * sizeof (*value_at_n[i]) );
        if (value_at_n[i] == NULL)
            exit (1);
        for (int j = 0; j < size+1; j++)
        {
            value_at_n [i][j] = (double **) malloc( 2 * sizeof (*value_at_n[i][j]) );
            if (value_at_n[i][j] == NULL)
                exit (1);
            for (int k = 0; k < 2; k++)
            {
                value_at_n [i][j][k] = (double *) malloc( 2 * sizeof (*value_at_n[i][j][k]) );
                if (value_at_n[i][j][k] == NULL)
                    exit (1);
                for (int l = 0; l < 2; l++)
                    value_at_n [i][j][k][l] = 10000.0;
            }
        }
    }

//    std::cout << "Length of array = " << (sizeof(value_at_n)/sizeof(value_at_n[0])) << std::endl;

    return value_at_n;
}

void free_4d_pointer (double **** p, int size)
{
    for (int i = 0; i < size+1; i++)
    {
        for (int j = 0; j < size+1; j++)
        {
            for (int k = 0; k < 2; k++)
            {
                free(p [i][j][k]);
            }
            free(p [i][j]);
        }
        free(p [i]);
    }
    free(p);
    p = nullptr;
}




#endif // DEFINITIONS_H
