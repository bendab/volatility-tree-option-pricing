#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "Definitions.h"
#include "Parameters.h"
#ifdef _MSC_VER
#include <ppl.h>
#endif
#include "fastExponential.h"
double roughSimulation (double K, double rate, double eta,
                        double kappa,double theta,
                        double rho, double T, double nu_0,
                        double S_0);
void Probabilities(double *IP, int k_1, int l, int m, int xi, int zeta);
double * Probabilities_old (int k_1, int l, int m, int xi, int zeta);
double * IPk_1 (double tk_1, double X_k_2, double X_k_1,
                double V_k_2, double V_k_1,
                double Y_k_2, double Y_k_1,
                double xi_k_1, double zeta_k_1);
double Epsilon (p2d * vk, double tk_1,
                double X_k_2, double X_k_1,
                double V_k_2, double V_k_1, double Y_k_2,
                double Y_k_1,
                double xi_k_1, double zeta_k_1);

inline double payoff (double t, double S, double K);
inline double Discount (double t);

void DKM (double *c_i, double * gamma_i, int n_ld);
rg_v2d * roughVolatility_Initialisation (double *c_i, double * gamma_i,
                                         int n, int n_ld);
double roughSimulation(double K, double rate, double eta,
                       double kappa, double theta,
                       double rho, double T, double nu_0,
                       double S_0)
{
    x_0 = std::log(S_0);
    y_0 = nu_0;

    int size = 2*n+1;
    double **** value_at_n = init_4d_pointer(size);
    double **** value_at_k = nullptr;// = init_4d_pointer(n+1);
    double **** value_at_k_1 = nullptr;

    //Final Value Vector

    /***Discrete Kernel initialization***/
    //    double c_i [n_ld];
    //    double gamma_i [n_ld];
    //    DKM (c_i, gamma_i, n_ld);
    /***Discrete Kernel initialization***/

    /***Rough Volatility Initialization***/
    //    rg_v2d * GY = roughVolatility_Initialisation (c_i, gamma_i, n+1, n_ld);
    /***Rough Volatility Initialization***/

    /***Vn computation***/
    //Initialization of the final points
    //l,m in [0,N]
    int const minus = 0;
    int const plus = 1;
    double Discount = exp(-rate*delta);//Discount Factor
    double alpha_n, Sn, hat_Xn;
    double Yn_1, Y_n;
    double tmp_payoff_n;
    //    double D_n = exp (-rate*(T-T));
    double D_n = exp ( -rate*(n)*delta );
    //We initiate the values at time n
    for (int l=-n; l<=n; l+=2)//Spot
    {
        double X_n = RV_Xi(x_0,l,n);
        for (int m=-n; m<=n; m+=2)//Vol
        {
            Y_n = RV_Zeta(y_0,m,n);
            for (int _xi_n = minus; _xi_n <= plus; _xi_n++)
            {
                for (int _zeta_n = minus; _zeta_n <= plus; _zeta_n++)
                {
                    double tmp_zeta = (2.0*(static_cast<double>(_zeta_n))-1.0);//=+/-1
                    Yn_1 = RV_Zeta (y_0, m-tmp_zeta, n-1);

                    alpha_n = alpha(n, Yn_1);//alpha_n=f(G*Y_[n-1])
                    double tmp_xi = (2.0*(static_cast<double>(_xi_n))-1.0);//=+/-1
                    hat_Xn = X_n + sqrt_delta*alpha_n*tmp_xi;
                    Sn = exp(hat_Xn);
                    //                                        tmp_payoff_n = max(Sn - K, 0.0);//hat xi
                    tmp_payoff_n = max(K*D_n - Sn, 0.0);//hat xi
                    //                    tmp_payoff_n = max(Sn*D_n - K, 0.0)/D_n;//hat xi
                    value_at_n [l+n][m+n][_xi_n][_zeta_n] = tmp_payoff_n;
                }
            }
        }
    }
    /***Vn computation***/

    //Then we execute the algorithm
    auto start = high_resolution_clock::now();

    value_at_k = value_at_n;
    //we pursue at time n-1
    double sum_init_new_vk = 0.0;
    double tmp_Epsilon_k;
    double * IP = new double [4];
    double IP_pp, IP_pm, IP_mp, IP_mm;
    for(int tk_1 = n-1; tk_1 >= 0; tk_1--)//time : n-2 means n-1
    {
        if (tk_1%100==0)
            cout << "loop = " << tk_1 << endl;

        //we re-initialize vk_1
        clock_t start_vk = clock();
        size = 2*tk_1;
        value_at_k_1 = init_4d_pointer(size);
        clock_t stop_vk = clock();
        sum_init_new_vk += static_cast<double>( (stop_vk - start_vk)/CLOCKS_PER_SEC );

        double t = (tk_1)*delta;
        double Dk_1 = exp( -rate*(t) );
        for(int l = -tk_1; l <= tk_1; l+=2)//S=price//k_1+1=k
        {
            double tmp_payoff_k_1;
            double alpha_k_1, Xk_1, Xk_2;
            double Y_lk_2, Y_lk_1;
            double Sk_1, hat_Xk_1;
            Xk_1 = RV_Xi(x_0, l, tk_1);//useless for EU option
            for(int m = -tk_1; m <= tk_1; m+=2)//Volatility
            {
                Y_lk_1 = RV_Zeta (y_0, m, tk_1);
                //(double[10][10][2][2])(*value_at_k);
                //double **** value_at_k
                int index = tk_1+1;
                double v_pp = value_at_k [l+index+1][m+index+1][plus][plus];
                double v_pm = value_at_k [l+index+1][m+index-1][plus][minus];
                double v_mp = value_at_k [l+index-1][m+index+1][minus][plus];
                double v_mm = value_at_k [l+index-1][m+index-1][minus][minus];

                //#pragma omp parallel for
                for (int _xi_k_1 = 0; _xi_k_1 < 2; _xi_k_1++)
                {
                    for (int _zeta_k_1 = 0; _zeta_k_1 < 2; _zeta_k_1++)
                    {
                        //                        if (tk_1 == 0 && (_xi_k_1 == plus || _zeta_k_1 == plus))
                        //                            continue;
                        /***MODIFIED***/
                        double tmp_zeta = (2.0*static_cast<double>(_zeta_k_1)-1.0);//=+/-1
                        Y_lk_2 = RV_Zeta (y_0, m-tmp_zeta, tk_1-1);
                        //                        double tmp_Y_lk_2 = Y_lk_1 - sqrt_delta*tmp_zeta;
                        /***MODIFIED***/

                        alpha_k_1 = alpha (tk_1, Y_lk_2);//alpha_{k-1}=f(V_[k-2])
                        Probabilities (IP, tk_1+1, l, m, _xi_k_1, _zeta_k_1);//Why tk_1+1 ?

                        IP_pp = IP[0];
                        IP_pm = IP[1];
                        IP_mp = IP[2];
                        IP_mm = IP[3];

                        //Discount = exp(-rate*t);

                        //GIRSANOV
                        //                        double Z_t = exp ( static_cast<double>(tmp_xi)*sqrt_delta - 0.5*delta );
                        //                        tmp_Epsilon_k = Z_t * (IP_pp*v_pp + IP_pm*v_pm + IP_mp*v_mp + IP_mm*v_mm) * Discount;
                        //GIRSANOV

                        tmp_Epsilon_k = IP_pp*v_pp + IP_pm*v_pm + IP_mp*v_mp + IP_mm*v_mm;
                        //US
                        double tmp_xi = (2.0*static_cast<double>(_xi_k_1)-1.0);//=+/-1
                        hat_Xk_1 = Xk_1 + sqrt_delta*alpha_k_1*tmp_xi;
                        Sk_1 = exp(hat_Xk_1);
                        tmp_payoff_k_1 = max (K*Dk_1 - Sk_1, 0.0);
                        value_at_k_1 [l+tk_1][m+tk_1][_xi_k_1][_zeta_k_1] = max(tmp_payoff_k_1, tmp_Epsilon_k);
                        //EU
                        //                        value_at_k_1 [l+tk_1][m+tk_1][_xi_k_1][_zeta_k_1] = tmp_Epsilon_k;
                    }
                }
            }
        }

        clock_t start_free_vk = clock();
        free_4d_pointer(value_at_k, 2*(tk_1+1));
        clock_t stop_free_vk = clock();
        sum_init_new_vk += (double) (stop_free_vk - start_free_vk)/CLOCKS_PER_SEC;

        value_at_k = value_at_k_1;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<seconds>(stop - start);
    cout << "Time taken by Algorithm: " <<
            duration.count() << " seconds" << endl;
    cout << "Time taken by new vk : " <<
            sum_init_new_vk << " seconds" << endl;

    /***t=0***/
    tmp_Epsilon_k = value_at_k_1 [0][0][minus][minus];
    //At the end zeta_0=xi_0=alpha_0=beta_0=0
    //            Probabilities (IP, 0, 0, 0, +1, +1);//==IP_{0,0,0,0,0}
    //            IP_pp = IP[0];//I have deleted IP
    //            IP_pm = IP[1];
    //            IP_mp = IP[2];
    //            IP_mm = IP[3];

    //        tmp_Epsilon_k = IP_pp*value_at_k_1 [2][2][plus][plus]
    //                + IP_pm*value_at_k_1 [2][0][plus][minus]
    //                + IP_mp*value_at_k_1 [0][2][minus][plus]
    //                + IP_mm*value_at_k_1 [0][0][minus][minus];

    cout << "pp = " << value_at_k_1[0][0][plus][plus] << endl;
    cout << "pm = " << value_at_k_1[0][0][plus][minus] << endl;
    cout << "mp = " << value_at_k_1[0][0][minus][plus] << endl;
    cout << "mm = " << value_at_k_1[0][0][minus][minus] << endl;

    //US
    //    double tmp_payoff_k_1 = max (K - S_0, 0.0);
    //    double Price = max(tmp_payoff_k_1, tmp_Epsilon_k)/**Discount*/;

    //EU
    double Price = tmp_Epsilon_k;

    free_4d_pointer(value_at_k, size);
    free (IP);

    return Price;
}

void Probabilities (double * IP, int k_1, int l, int m, int xi, int zeta)
{
    double IE_zeta_k, IE_xi_k, X_cste;
    //    double IP_plus_k;

    double Yk_1 = RV_Zeta (y_0, m, k_1);

    //    double Yk_1 = y_0 + sqrt_delta*static_cast<double>(m+k_1);

    /***MODIFIED***/
    int zeta_k_1 = static_cast<int>(2.0*static_cast<double>(zeta) - 1.0);//=+/-1
    double Yk_2 = RV_Zeta (y_0, m-zeta_k_1, k_1-1);
    //    double Yk_2 = Yk_1 - zeta_k_1*sqrt_delta;//uncomment
    /***MODIFIED***/

    double Xk_1 = RV_Xi (x_0, l, k_1);
    /***MODIFIED***/
    int xi_k_1 = static_cast<int>(2.0*static_cast<double>(xi) - 1.0);//=+/-1
    double Xk_2 = RV_Xi (x_0, l-xi_k_1, k_1-1);
    //    double X_k_2 = Xk_1 - static_cast<double>(xi_k_1)*sqrt_delta;//uncomment
    /***MODIFIED***/

    double alpha_k_1 = alpha (k_1-1, Yk_2);
    double beta_k_1 = beta (k_1-1, Yk_2);

    double alpha_k = alpha (k_1, Yk_1);
    double beta_k = beta (k_1, Yk_1);

    //hat
    //    Xk_1 = Xk_1 + sqrt_delta*alpha_k_1*xi_k_1;
    //    Yk_1 = Yk_1 + sqrt_delta*beta_k_1*zeta_k_1;
    //hat

    double mux_k_1 = mu_x(k_1, Xk_2, Yk_2);//predictable
    double muy_k_1 = mu_y(k_1, Yk_2);

    double sigmax_k_1 = sigma_x (k_1, Yk_2);
    double sigmay_k_1 = sigma_y (k_1, Yk_2);


    //**Soner Version**//
    //IP_k(xi_k=+1.0)
    //    double p_plus_k = max(0.0,
    //                         min(1.0,  (
    //                exp(rate*delta + sqrt_delta*alpha_k_1*xi_k_1)
    //                - exp(-sqrt_delta*(alpha_k+1.0f))
    //                )
    //            /( exp(sqrt_delta*(alpha_k+1.0f)) - exp(-sqrt_delta*(alpha_k+1.0f)) ) ));
    //    double q_plus_k = max(0.0,
    //                          min(1.0, 0.5 + (alpha_k_1*zeta_k_1)/(2.0*(1.0+alpha_k))
    //                     + (sqrt_delta*muy_k_1)/(2.0*(1.0+alpha_k)) ));


    //    //    //IP_{k-1}(xi_k=+1,zeta_k=+1)=++
    //        IP[0] = p_plus_k*q_plus_k;//In general put min (1.0f max (0.0f, ...))
    //    //    //IP_{k-1}(xi_k=+1,zeta_k=-1)=+-
    //        IP[1] = p_plus_k*(1.0-q_plus_k);//exception: because we have put sqrt(max(0.0f, ...))
    //    //    //IP_{k-1}(xi_k=-1,zeta_k=+1)=-+
    //        IP[2] = (1.0-p_plus_k)*q_plus_k;
    //    //    //IP_{k-1}(xi_k=-1,zeta_k=-1)=--
    //        IP[3] = (1.0-p_plus_k)*(1.0-q_plus_k);
    //**Soner Version**//

    ///RISK PROBA
    //    IP_plus_k = (
    //                exp(rate*delta + sqrt_delta*alpha_k_1*xi_k_1)
    //                - exp(-sqrt_delta*(alpha_k+1.0))
    //                )
    //            /( exp(sqrt_delta*(alpha_k+1.0)) - exp(-sqrt_delta*(alpha_k+1.0)) );
    ///RISK PROBA
    ///


    //***GENERAL VERSION***//
    //!!DO NOT DELETE !!!//
    //        IE_xi_k = (mux_k_1*sqrt_delta + static_cast<double>(xi_k_1)*alpha_k_1)/(alpha_k+1.0);//tilde zeta_k//!!DO NOT DELETE !!!//GENERAL XI VERSION SEE DOLINSKY REMARK
    //        IE_zeta_k = (muy_k_1*sqrt_delta + static_cast<double>(zeta_k_1)*beta_k_1)/(beta_k+1.0);//tilde zeta_k
    //        //    IE_xi_k = 2.0f*IP_plus_k-1.0f;//tilde xi_k
    //        double X = (sigmax_k_1*sigmay_k_1*rho - alpha_k_1*beta_k_1*static_cast<double>(xi_k_1)*static_cast<double>(zeta_k_1))
    //                /((alpha_k+1.0)*(beta_k+1.0))
    //                + (beta_k_1*static_cast<double>(zeta_k_1)*IE_xi_k)/(beta_k+1.0)
    //                + (alpha_k_1*static_cast<double>(xi_k_1)*IE_zeta_k)/(alpha_k+1.0);
    //        //******GOOOD*******//
    //        //IP_{k-1}(xi_k=+1,zeta_k=+1)=++
    //        IP[0] = 0.25*(1.0 + IE_zeta_k + IE_xi_k + X);//In general put min (1.0f max (0.0f, ...))
    //        //IP_{k-1}(xi_k=+1,zeta_k=-1)=+-
    //        IP[1] = 0.25*(1.0 - IE_zeta_k + IE_xi_k - X);//exception: because we have put sqrt(max(0.0f, ...))
    //        //IP_{k-1}(xi_k=-1,zeta_k=+1)=-+
    //        IP[2] = 0.25*(1.0 + IE_zeta_k - IE_xi_k - X);
    //        //IP_{k-1}(xi_k=-1,zeta_k=-1)=--
    //        IP[3] = 0.25*(1.0 - IE_zeta_k - IE_xi_k + X);
    //******GOOOD*******//
    //***GENERAL VERSION***//
    //!!DO NOT DELETE !!!//


    //RISK NEUTRAL VERSION
    double exp_plus_rate_delta = 1.0;//exp( +rate*delta );//PROBLEM
    double exp_Delta_hatXk_plus = exp( sqrt_delta*((alpha_k+1.0)*(+1.0) - alpha_k_1*static_cast<double>(xi_k_1)) );
    double exp_Delta_hatXk_minus = exp( sqrt_delta*((alpha_k+1.0)*(-1.0) - alpha_k_1*static_cast<double>(xi_k_1)) );

    IE_xi_k = (mux_k_1*sqrt_delta + static_cast<double>(xi_k_1)*alpha_k_1)/(alpha_k+1.0);
    IE_zeta_k = (muy_k_1*sqrt_delta + static_cast<double>(zeta_k_1)*beta_k_1)/(beta_k+1.0);//tilde zeta_k
    X_cste = (sigmax_k_1*sigmay_k_1*rho - alpha_k_1*beta_k_1*static_cast<double>(xi_k_1)*static_cast<double>(zeta_k_1))
            /((alpha_k+1.0)*(beta_k+1.0))
            + (beta_k_1*static_cast<double>(zeta_k_1)*IE_xi_k)/(beta_k+1.0)
            + (alpha_k_1*static_cast<double>(xi_k_1)*IE_zeta_k)/(alpha_k+1.0);

    //ans = p_pp
    IP[0] = (IE_zeta_k*exp_Delta_hatXk_plus + X_cste*exp_Delta_hatXk_plus
             + IE_zeta_k*exp_Delta_hatXk_minus + 2.0*IE_xi_k*exp_Delta_hatXk_minus
             + X_cste*exp_Delta_hatXk_minus + 2.0*exp_plus_rate_delta)
            /(4.0*(exp_Delta_hatXk_plus + exp_Delta_hatXk_minus));
    //ans = p_pm
    IP[1] = -(IE_zeta_k*exp_Delta_hatXk_plus + X_cste*exp_Delta_hatXk_plus
              + IE_zeta_k*exp_Delta_hatXk_minus - 2.0*IE_xi_k*exp_Delta_hatXk_minus
              + X_cste*exp_Delta_hatXk_minus - 2.0*exp_plus_rate_delta)
            /(4.0*(exp_Delta_hatXk_plus + exp_Delta_hatXk_minus));
    //ans = p_mp
    IP[2] = (IE_zeta_k*exp_Delta_hatXk_plus - 2.0*IE_xi_k*exp_Delta_hatXk_plus
             - X_cste*exp_Delta_hatXk_plus + IE_zeta_k*exp_Delta_hatXk_minus
             - X_cste*exp_Delta_hatXk_minus + 2.0*exp_plus_rate_delta)
            /(4.0*(exp_Delta_hatXk_plus + exp_Delta_hatXk_minus));
    //ans = p_mm
    IP[3] = (- IE_zeta_k*exp_Delta_hatXk_plus - 2.0*IE_xi_k*exp_Delta_hatXk_plus
             + X_cste*exp_Delta_hatXk_plus - IE_zeta_k*exp_Delta_hatXk_minus
             + X_cste*exp_Delta_hatXk_minus + 2.0*exp_plus_rate_delta)
            /(4.0*(exp_Delta_hatXk_plus + exp_Delta_hatXk_minus));
    //***//RISK NEUTRAL VERSION***//


    ///!!!! Z_T !!!!////
    //    double exp_Z_t_plus = exp (-static_cast<double>(xi_k_1)*(+1.0)*alpha_k_1/(alpha_k+1.0)
    //                               -0.5*pow(alpha_k_1/(alpha_k+1.0),2));
    //    double exp_Z_t_minus = exp (-static_cast<double>(xi_k_1)*(-1.0)*alpha_k_1/(alpha_k+1.0)
    //                                -0.5*pow(alpha_k_1/(alpha_k+1.0),2));
    //        double exp_Z_t_plus = exp (-rate*delta - sqrt_delta*(+1.0)*(alpha_k+1.0)
    //                                   + sqrt_delta*static_cast<double>(xi_k_1)*alpha_k_1);
    //        double exp_Z_t_minus = exp (-rate*delta - sqrt_delta*(-1.0)*(alpha_k+1.0)
    //                                    + sqrt_delta*static_cast<double>(xi_k_1)*alpha_k_1);
    //        /////GET RID OF Z_T !!!!////
    //        //IP_{k-1}(xi_k=+1,zeta_k=+1)=++
    //        IP[0] = 0.25*(1.0 + IE_zeta_k + IE_xi_k + X)*exp_Z_t_plus;//In general put min (1.0f max (0.0f, ...))
    //        //IP_{k-1}(xi_k=+1,zeta_k=-1)=+-
    //        IP[1] = 0.25*(1.0 - IE_zeta_k + IE_xi_k - X)*exp_Z_t_plus;//exception: because we have put sqrt(max(0.0f, ...))
    //        //IP_{k-1}(xi_k=-1,zeta_k=+1)=-+
    //        IP[2] = 0.25*(1.0 + IE_zeta_k - IE_xi_k - X)*exp_Z_t_minus;
    //        //IP_{k-1}(xi_k=-1,zeta_k=-1)=--
    //        IP[3] = 0.25*(1.0 - IE_zeta_k - IE_xi_k + X)*exp_Z_t_minus;
    ///!!!! Z_T !!!!////


    //    double t1 = (alpha_k_1*xi_k_1 + mux_k_1*sqrt_delta)
    //            /(4.0*(1.0 + alpha_k));
    //    double t2 = (beta_k_1*zeta_k_1 + muy_k_1*sqrt_delta)
    //            /(4.0*(1.0 + beta_k));
    //    double t3 = (rho*sigmax_k_1*sigmay_k_1 + alpha_k_1*beta_k_1*xi_k_1*zeta_k_1)
    //            /(4.0*(1.0 + alpha_k)*(1.0 + beta_k));

    //    //IP_{k-1}(xi_k=+1,zeta_k=+1)=
    //    IP[0] = (0.25 + t1 + t2 + t3);//In general put min (1.0f max (0.0f, ...))
    //    //IP_{k-1}(xi_k=+1,zeta_k=-1)=
    //    IP[1] = (0.25 + t1 - t2 - t3);//exception: because we have put sqrt(max(0.0f, ...))
    //    //IP_{k-1}(xi_k=-1,zeta_k=+1)=
    //    IP[2] = (0.25 - t1 + t2 - t3);
    //    //IP_{k-1}(xi_k=-1,zeta_k=-1)=
    //    IP[3] = (0.25 - t1 - t2 + t3);

    //    //Truncation
    //    //IP_{k-1}(xi_k=+1,zeta_k=+1)=
    //    IP[0] = max(0.0, min(1.0, IP[0]));//In general put min (1.0f max (0.0f, ...))
    //    //IP_{k-1}(xi_k=+1,zeta_k=-1)=
    //    IP[1] = max(0.0, min(1.0, IP[1]));//exception: because we have put sqrt(max(0.0f, ...))
    //    //IP_{k-1}(xi_k=-1,zeta_k=+1)=
    //    IP[2] = max(0.0, min(1.0, IP[2]));
    //    //IP_{k-1}(xi_k=-1,zeta_k=-1)=
    //    IP[3] = max(0.0, min(1.0, IP[3]));


    double sum = IP[0] + IP[1] + IP[2] + IP[3];
    //    if (sum <= 1.01 && sum >= 0.98)
    {
        IP[0] = IP[0]/sum;
        IP[1] = IP[1]/sum;
        IP[2] = IP[2]/sum;
        IP[3] = IP[3]/sum;
    }
    //    sum = IP[0] + IP[1] + IP[2] + IP[3];
    //    cout << "P = "<< IP[0] + IP[1] + IP[2] << endl;
    //    sum = IP[0] + IP[1] + IP[2] + IP[3];
}

inline double payoff (double t, double S, double K)
{
    return Discount(t)*max (0.0, K-S);
}

inline double Discount (double t)
{
    return exp(-rate*t);
}

//xi=0/1 and we convert it to xi_k_1=+/-1
double * Probabilities_old (int k_1, int l, int m, int xi, int zeta)
{
    double t1, t2, t3;
    double * IP = new double [4];

    double zeta_k_1 = 2.0f*((double) zeta)-1.0f;//=+/-1
    double Yk_1 = RV_Zeta (y_0,m,k_1);
    double Yk_2 = Yk_1 - zeta_k_1*sqrt_delta;

    double xi_k_1 = 2.0f*((double) xi)-1.0f;//=+/-1
    double Xk_1 = RV_Xi (x_0,l,k_1);
    double X_k_2 = Xk_1 - xi_k_1*sqrt_delta;

    double alpha_k_1 = alpha (k_1-1.0f, Yk_2);
    double beta_k_1 = beta (k_1-1.0f, Yk_2);

    double alpha_k = alpha (k_1, Yk_1);
    double beta_k = beta (k_1, Yk_1);

    double mux_k_1 = mu_x(k_1-1.0f, X_k_2, Yk_2);//predictable
    double muy_k_1 = mu_y(k_1-1.0f, Yk_2);

    double sigmax_k_1 = sigma_x (k_1-1.0f, Yk_2);
    double sigmay_k_1 = sigma_y (k_1-1.0f, Yk_2);

    t1 = (alpha_k_1*xi_k_1 + mux_k_1*sqrt_delta)
            /(4.0f*(1.0f + alpha_k));
    t2 = (beta_k_1*zeta_k_1 + muy_k_1*sqrt_delta)
            /(4.0f*(1.0f + beta_k));
    t3 = (rho*sigmax_k_1*sigmay_k_1 + alpha_k_1*beta_k_1*xi_k_1*zeta_k_1)
            /(4.0f*(1.0f + alpha_k)*(1.0f + beta_k));

    //In order to get a Risk neutral Probability
    double xi_k_plus = +1.0f;
    double xi_k_minus = -1.0f;
    double tmp_plus = + rate*delta
            - sqrt_delta*(alpha_k+1.0f)*(+1.0f)
            + sqrt_delta*alpha_k_1*xi_k_1;
    double tmp_minus = + rate*delta
            - sqrt_delta*(alpha_k+1.0f)*(-1.0f)
            + sqrt_delta*alpha_k_1*xi_k_1;
    //    double Delta_Z_k_1_plus = fastExpo(20, tmp_plus);
    //    double Delta_Z_k_1_minus = fastExpo(20, tmp_minus);
    double Delta_Z_k_1_plus = 1.0;//exp(tmp_plus);
    double Delta_Z_k_1_minus = 1.0;//exp(tmp_minus);
    //IP_{k-1}(xi_k=+1,zeta_k=+1)=
    IP[0] = (0.25f + t1 + t2 + t3)*Delta_Z_k_1_plus;//In general put min (1.0f max (0.0f, ...))
    //IP_{k-1}(xi_k=+1,zeta_k=-1)=
    IP[1] = (0.25f + t1 - t2 - t3)*Delta_Z_k_1_plus;//exception: because we have put sqrt(max(0.0f, ...))
    //IP_{k-1}(xi_k=-1,zeta_k=+1)=
    IP[2] = (0.25f - t1 + t2 - t3)*Delta_Z_k_1_minus;
    //IP_{k-1}(xi_k=-1,zeta_k=-1)=
    IP[3] = (0.25f - t1 - t2 + t3)*Delta_Z_k_1_minus;

    //Truncation
    //    //IP_{k-1}(xi_k=+1,zeta_k=+1)=
    IP[0] = max(0.0, min(1.0, IP[0]));//In general put min (1.0f max (0.0f, ...))
    //IP_{k-1}(xi_k=+1,zeta_k=-1)=
    IP[1] = max(0.0, min(1.0, IP[1]));//exception: because we have put sqrt(max(0.0f, ...))
    //IP_{k-1}(xi_k=-1,zeta_k=+1)=
    IP[2] = max(0.0, min(1.0, IP[2]));
    //IP_{k-1}(xi_k=-1,zeta_k=-1)=
    IP[3] = max(0.0, min(1.0, IP[3]));

    double sum = IP[0] + IP[1] + IP[2] + IP[3];
    IP[0] = IP[0]/sum;
    IP[1] = IP[1]/sum;
    IP[2] = IP[2]/sum;
    IP[3] = IP[3]/sum;
    sum = IP[0] + IP[1] + IP[2] + IP[3];
    //    cout << "P = "<< IP[0] + IP[1] + IP[2] << endl;

    return IP;//I have to delete it at the end
}

/***Epsilon***/
/**
 * @brief Epsilon see Article
 * @param vk final Value
 * @param X_k_1
 * @param X_k
 * @param GY_k_1
 * @param GY_k
 * @param Y_k_1
 * @param Y_k
 * @param xi_k_1
 * @param zeta_k_1
 * @return
 */
double Epsilon (p2d * vk, double tk_1,
                double X_k_2, double X_k_1,
                double V_k_2, double V_k_1,
                double Y_k_2, double Y_k_1,
                double xi_k_1, double zeta_k_1)
{

    double * IP = IPk_1(tk_1,
                        X_k_2, X_k_1,
                        V_k_2, V_k_1,
                        Y_k_2, Y_k_1,
                        xi_k_1, zeta_k_1);
    double IE = IP[0]*(*vk)[+1][+1] + IP[1]*(*vk)[+1][-1]
            + IP[2]*(*vk)[-1][+1] + IP[3]*(*vk)[-1][-1];

    delete IP;
    return IE;
}

/**
 * @brief Probabilities
 * @param X_k_1
 * @param X_k
 * @param V_k_1
 * @param V_k
 * @param Y_k_1
 * @param Y_k
 * @param xi_k_1
 * @param zeta_k_1
 * @return
 */
double * IPk_1 (double tk_1,
                double X_k_2, double X_k_1,
                double V_k_2, double V_k_1,
                double Y_k_2, double Y_k_1,
                double xi_k_1, double zeta_k_1)
{
    double t1, t2, t3;
    double * IP = new double [4];

    double alpha_k_1 = alpha (tk_1-1, V_k_2);
    double beta_k_1 = beta (tk_1-1, Y_k_2);

    double alpha_k = alpha (tk_1, V_k_1);
    double beta_k = beta (tk_1, Y_k_1);

    double mux = mu_x(tk_1-1, X_k_2, V_k_2);
    double muy = mu_y(tk_1-1, Y_k_2);

    double sigmax = sigma_x (tk_1-1, V_k_2);
    double sigmay = sigma_y (tk_1-1, Y_k_2);

    t1 = (alpha_k_1*xi_k_1 + mux*sqrt_delta)
            /(4.0f*(1.0f + alpha_k));
    t2 = (beta_k_1*zeta_k_1 + muy*sqrt_delta)
            /(4.0f*(1.0f + beta_k));
    t3 = (rho*sigmax*sigmay + alpha_k_1*beta_k_1*xi_k_1*zeta_k_1)
            /(4.0f*(1.0f + alpha_k)*(1.0f + beta_k));

    //IP_{k-1}(xi_k=+1,zeta_k=+1)=
    IP[0] = min(1.0, max(0.0, 0.25 + t1 + t2 + t3));
    //IP_{k-1}(xi_k=+1,zeta_k=-1)=
    IP[1] = min(1.0, max(0.0, 0.25 + t1 - t2 - t3));
    //IP_{k-1}(xi_k=-1,zeta_k=+1)=
    IP[2] = min(1.0, max(0.0, 0.25 - t1 + t2 - t3));
    //IP_{k-1}(xi_k=-1,zeta_k=-1)=
    IP[3] = min(1.0, max(0.0, 0.25 - t1 - t2 + t3));

    return IP;
}

/***Epsilon***/

/**
 * @brief DKM Discrete Kernel Initialization : The goal is
 * to init pi and c_i for the discrete Laplace
 * @param c_i
 * @param gamma_i
 * @param n_ld size of the arrays c_i and gamma_i.
 * It is the size of the discretization of the Laplace Integral.
 */
void DKM (double *c_i, double * gamma_i, int n_ld)
{
    double pi = pi_m(n_ld);
    c_i [0] = c_i [1] = 0.0f;// == 1.0f/n_ld
    gamma_i [0] = gamma_i [1] = 0.0f;
    for (int i = 2; i < n_ld; i++)
    {
        if (H != 0.5)
        {
            c_i [i] = c (i*pi, (i-1)*pi);
            gamma_i [i] = gamma (c_i [i], i*pi, (i-1)*pi);
        }
        else // It is a classic Brownian Motion
        {
            c_i [i] = 0.0f;
            gamma_i [i] = 0.0f;
        }
    }
}


/**
 * @brief roughVolatility_Initialisation It initializes the rough volatility G*Y.
 * It is a forward initilization. GY_k return GxY_{k+1} so GxY is predictable, and,
 * GY[k][l][-/+1] means at node (k,l) with k=time, l=space, we move up (+1) or down (-1),
 * So :
 * GY[k][l][-/+1] gives the value of GxY_{k+1}(l-1) if we move up +1, (Draw a binary tree to see l-1),
 * GY[k][l][-/+1] gives the value of GxY_{k+1}(l) if we move down -1.
 * It explain the predictability, also the forward construction, and, why we initialize with n+1 rather than n (as the other arrays)
 * GxY is initialized by a Multifactor construction.
 * @param GY Rough volatility G*Y where Y is the volatility and G the kernel
 * @param c_i parameter
 * @param gamma_i parameter
 * @param n_ld size of the arrays c_i and gamma_i.
 * It is the size of the discretization of the Laplace Integral.
 */
rg_v2d * roughVolatility_Initialisation (double *c_i, double * gamma_i,
                                         int n, int n_ld)
{
    auto start_RV = high_resolution_clock::now();

    //int m = n_ld;
    if (H >= 0.5)
        n_ld = 1;
    //Multifactor
    rg_v2d * Vni_k;
    rg_v2d * Vnik_1 = new rg_v2d(1, v1d(n_ld));//1 because at begining, time = 0
    //Multifactor

    //Rough Volatility
    rg_v2d * GY = new rg_v2d(n);
    for (int time = 0 ; time < n; time++)
    {
        (*GY)[time] = v1d(time+1);//time+1 nodes per time
    }
    //Rough Volatility

    //The goal of these loops is
    //to initialize the rough volatility GY
    //By a Multifactor construction
    //    double Delta_gn_k;
    double gn_k, _sum;

    /**INIT**/
    //we initialize at time 0, see (3.2".5") in AEJJ
    (*GY)[0][0] = v_0;
    for (int i = 0 ; i < n_ld; i++)
        (*Vnik_1) [0][i] = 0.0f;
    /**INIT**/

    for (int time = 1; time < n; time++)//t = time
    {
        //uk new since we have deleted it in the last instruction
        //or never instanciated if it is the first passage
        //time+2 = number of nodes [0,time+1]
        Vni_k = new rg_v2d (time+1, v1d(n_ld));
        //we finish l==time because Y(time+1)
        //Delta_gn_k = gn(time, gamma_i, c_i, n_ld) - gn(time-1, gamma_i, c_i, n_ld);
        //gn_k = gn(time, gamma_i, c_i, n_ld);
        for (int l = 0; l <= time; l++)//l = node/space
        {
            _sum = gn_k;
            if(H < 0.5)//ROUGH
            {
                for (int i = 0; i < n_ld; i++)//i = Laplace discretization
                {
                    //recombining tree
                    if (l == time)
                        (*Vni_k) [l][i] = (1.0f-gamma_i[i]) * (*Vnik_1) [l-1][i] + sqrt_delta*(-1.0f);//Last node it is a down
                    else
                        (*Vni_k) [l][i] = (1.0f-gamma_i[i]) * (*Vnik_1) [l][i] + sqrt_delta*(+1.0f);//zeta=+1
                    _sum += c_i[i] * (*Vni_k) [l][i];
                }
            }
            else if(H >= 0.5)//NOT ROUGH
            {
                if (l == time)
                    (*Vni_k) [l][0] = (*Vnik_1) [l-1][0] + sqrt_delta*(-1.0f);//zeta=+1
                else
                    (*Vni_k) [l][0] = (*Vnik_1) [l][0] + sqrt_delta*(+1.0f);//zeta=+1
                _sum += (*Vni_k) [l][0];
            }
            (*GY) [time][l] = _sum;
        }
        //delete forward
        delete Vnik_1;
        Vnik_1 = Vni_k;
    }
    delete Vni_k;

    auto stop_RV = high_resolution_clock::now();
    auto duration_RV = duration_cast<seconds>(stop_RV - start_RV);
    cout << "Rough Volatility Init: " << duration_RV.count() << " seconds" << endl;

    return GY;
}

#endif // ALGORITHM_H
