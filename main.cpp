#include "Algorithm.h"
//#include "FFT.h"
//#include <Eigen/Dense>

int main()
{
    if (2.0*kappa*theta <= eta*eta)
    {
      std::cout << "Feller condition is not validated" << endl;
      return EXIT_FAILURE;
    }

    std::cout << "n = " << n << ", ";
    std::cout << "n_ld = " << n_ld << endl;

    cout << "T = " << T << ", ";
    std::cout << "H = " << H << ", ";
    std::cout << "K = " << K << ", ";
    std::cout << "r = " << rate << ", ";
    std::cout << "eta = " << eta << ", ";
    std::cout << "kappa = " << kappa << ", ";
    std::cout << "theta = " << theta << endl;

    std::cout << "S0 = " << S_0 << ", ";
    std::cout << "rho = " << rho << ", ";
    std::cout << "sqrt(nu0) = " << sqrt(nu_0) << endl;

    double result = roughSimulation (K, rate, eta, kappa, theta,
                                     rho, T, nu_0, S_0);

    std::cout << "result = " << result << std::endl;

    return EXIT_SUCCESS;
}
