TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp
QMAKE_CXXFLAGS += -DNDEBUG -DBOOST_UBLAS_NDEBUG -fopenmp

LIBS += -fopenmp

win32 {
    INCLUDEPATH += E:/boost/boost_1_72_0
    INCLUDEPATH += E:/eigen
    INCLUDEPATH += C:/boost/boost_1_72_0
    INCLUDEPATH += C:/eigen
}

HEADERS += \
    Algorithm.h \
    Definitions.h \
    EuropeanHeston.h \
    Junk.h \
    Parameters.h \
    fastExponential.h
