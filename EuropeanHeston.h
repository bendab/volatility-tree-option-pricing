#ifndef EUROPEANHESTON_H
#define EUROPEANHESTON_H

#include "Definitions.h"
#include "Parameters.h"
#ifdef _MSC_VER
#include <ppl.h>
#endif
#include "fastExponential.h"

double HestonEUSimulation (double K, double rate, double eta,
                        double kappa,double theta,
                        double rho, double T, double nu_0,
                        double S_0);

double HestonEUSimulation(double K, double rate, double eta,
                       double kappa, double theta,
                       double rho, double T, double nu_0,
                       double S_0)
{
    x_0 = std::log(S_0);
    y_0 = nu_0;//To define !!!

    int size = 2*n+1;
    double **** value_at_n = init_4d_pointer(size);
    double **** value_at_k = nullptr;// = init_4d_pointer(n+1);
    double **** value_at_k_1 = nullptr;

    //Final Value Vector

    /***Discrete Kernel initialization***/
    //    double c_i [n_ld];
    //    double gamma_i [n_ld];
    //    DKM (c_i, gamma_i, n_ld);
    /***Discrete Kernel initialization***/

    /***Rough Volatility Initialization***/
    //    rg_v2d * GY = roughVolatility_Initialisation (c_i, gamma_i, n+1, n_ld);
    /***Rough Volatility Initialization***/

    /***Vn computation***/
    //Initialization of the final points
    //l,m in [0,N]
    int const minus = 0;
    int const plus = 1;
    double Discount = exp(-rate*delta);//Discount Factor
    double alpha_n, Sn, hat_Xn;
    double Yn_1, Y_n;
    double tmp_payoff_n = 0.0;
    //    double D_n = exp (-rate*(T-T));
    double D_n = exp (-rate*(T));
    //We initiate the values at time n
    for (int l=-n; l<=n; l+=2)//Spot
    {
        double X_n = RV_Xi(x_0,l,n);
        for (int m=-n; m<=n; m+=2)//Vol
        {
            Y_n = RV_Zeta(y_0,m,n);
            for (int _xi_n = minus; _xi_n <= plus; _xi_n++)
            {
                for (int _zeta_n = minus; _zeta_n <= plus; _zeta_n++)
                {
                    double tmp_zeta = (2.0*(static_cast<double>(_zeta_n))-1.0);//=+/-1
                    Yn_1 = RV_Zeta (y_0, m-tmp_zeta, n-1);

                    alpha_n = alpha(n-1, Yn_1);//alpha_n=f(G*Y_[n-1])
                    double tmp_xi = (2.0*(static_cast<double>(_xi_n))-1.0);//=+/-1
                    hat_Xn = X_n + sqrt_delta*alpha_n*tmp_xi;
                    Sn = exp(hat_Xn);
                    //                    tmp_payoff_n = max(K - Sn, 0.0);//hat xi
                    tmp_payoff_n = max(K - Sn, 0.0)* D_n;//hat xi
                    value_at_n [l+n][m+n][_xi_n][_zeta_n] = tmp_payoff_n;
                }
            }
        }
    }
    /***Vn computation***/

    //Then we execute the algorithm
    auto start = high_resolution_clock::now();

    value_at_k = value_at_n;
    //we pursue at time n-1
    double sum_init_new_vk = 0.0;
    double tmp_Epsilon_k;
    double * IP = new double [4];
    double IP_pp, IP_pm, IP_mp, IP_mm;
    for(int tk_1 = n-1; tk_1 >= 0; tk_1--)//time : n-2 means n-1
    {
        if (tk_1%100==0)
            cout << "loop = " << tk_1 << endl;

        //we re-initialize vk_1
        clock_t start_vk = clock();
        size = 2*tk_1+1;
        value_at_k_1 = init_4d_pointer(size);
        clock_t stop_vk = clock();
        sum_init_new_vk += (double) (stop_vk - start_vk)/CLOCKS_PER_SEC;

        double t = (tk_1)*delta;
        double Dk_1 = exp( -rate*(t) );
        for(int l = -tk_1; l <= tk_1; l+=2)//S=price//k_1+1=k
        {
            double tmp_payoff_k_1;
            double alpha_k_1, Xk_1, X_lk_2;
            double Y_lk_2, Y_lk_1;
            double Sk_1, hat_Xk_1;
            Xk_1 = RV_Xi(x_0, l, tk_1);
            for(int m = -tk_1; m <= tk_1; m+=2)//Volatility
            {
                Y_lk_1 = RV_Zeta (y_0, m, tk_1);
                //(double[10][10][2][2])(*value_at_k);
                //double **** value_at_k
                int index = tk_1+1;
                double v_pp = value_at_k [l+index+1][m+index+1][plus][plus];
                double v_pm = value_at_k [l+index+1][m+index-1][plus][minus];
                double v_mp = value_at_k [l+index-1][m+index+1][minus][plus];
                double v_mm = value_at_k [l+index-1][m+index-1][minus][minus];

                //#pragma omp parallel for
                for (int _xi_k_1 = 0; _xi_k_1 < 2; _xi_k_1++)
                {
                    for (int _zeta_k_1 = 0; _zeta_k_1 < 2; _zeta_k_1++)
                    {
                        //                        if (tk_1 == 0 && (_xi_k_1 == plus || _zeta_k_1 == plus))
                        //                            continue;
                        /***MODIFIED***/
                        double tmp_zeta = (2.0*static_cast<double>(_zeta_k_1)-1.0);//=+/-1
                        Y_lk_2 = RV_Zeta (y_0, m-tmp_zeta, tk_1-1);
                        //                        double tmp_Y_lk_2 = Y_lk_1 - sqrt_delta*tmp_zeta;
                        /***MODIFIED***/

                        alpha_k_1 = alpha (tk_1-1, Y_lk_2);//alpha_{k-1}=f(V_[k-2])
                        //Probabilities (IP, tk_1, l, m, _xi_k_1, _zeta_k_1);

                        IP_pp = IP[0];
                        IP_pm = IP[1];
                        IP_mp = IP[2];
                        IP_mm = IP[3];

                        //Discount = exp(-rate*t);
                        double tmp_xi = (2.0*static_cast<double>(_xi_k_1)-1.0);//=+/-1
                        hat_Xk_1 = Xk_1 + sqrt_delta*alpha_k_1*tmp_xi;

                        //GIRSANOV
                        //                        double Z_t = exp ( static_cast<double>(tmp_xi)*sqrt_delta - 0.5*delta );
                        //                        tmp_Epsilon_k = Z_t * (IP_pp*v_pp + IP_pm*v_pm + IP_mp*v_mp + IP_mm*v_mm) * Discount;
                        //GIRSANOV

                        tmp_Epsilon_k = (IP_pp*v_pp + IP_pm*v_pm + IP_mp*v_mp + IP_mm*v_mm);
                        Sk_1 = exp(hat_Xk_1);
                        //                        tmp_payoff_k_1 = max (K - Sk_1, 0.0);
                        tmp_payoff_k_1 = max (K - Sk_1, 0.0) * Dk_1;
                        value_at_k_1 [l+tk_1][m+tk_1][_xi_k_1][_zeta_k_1] = max(tmp_payoff_k_1, tmp_Epsilon_k);
                    }
                }
            }
        }

        clock_t start_free_vk = clock();
        free_4d_pointer(value_at_k, 2*(tk_1+1)+1);
        clock_t stop_free_vk = clock();
        sum_init_new_vk += (double) (stop_free_vk - start_free_vk)/CLOCKS_PER_SEC;

        value_at_k = value_at_k_1;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<seconds>(stop - start);
    cout << "Time taken by Algorithm: " <<
            duration.count() << " seconds" << endl;
    cout << "Time taken by new vk : " <<
            sum_init_new_vk << " seconds" << endl;

    /***t=0***/
    tmp_Epsilon_k = value_at_k_1 [0][0][plus][plus];
    //At the end zeta_0=xi_0=alpha_0=beta_0=0
    //        double * IP = Probabilities (0, 0, 0, 0, 0);//==IP_{0,0,0,0,0}
    //        double IP_pp = IP[0];//I have deleted IP
    //        double IP_pm = IP[1];
    //        double IP_mp = IP[2];
    //        double IP_mm = IP[3];

    //    tmp_Epsilon_k = IP_pp*value_at_k_1 [0][0][plus][plus]
    //            + IP_pm*value_at_k_1 [0][0][plus][minus]
    //            + IP_mp*value_at_k_1 [0][0][minus][plus]
    //            + IP_mm*value_at_k_1 [0][0][minus][minus];

    cout << "pp = " << value_at_k_1[0][0][plus][plus] << endl;
    cout << "pm = " << value_at_k_1[0][0][plus][minus] << endl;
    cout << "mp = " << value_at_k_1[0][0][minus][plus] << endl;
    cout << "mm = " << value_at_k_1[0][0][minus][minus] << endl;

    double tmp_payoff_k_1 = max (K - S_0, 0.0);
    double Price = max(tmp_payoff_k_1, tmp_Epsilon_k)/**Discount*/;

    free_4d_pointer(value_at_k, size);
    free (IP);

    return Price;
}
#endif // EUROPEANHESTON_H
