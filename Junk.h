#ifndef JUNK_H
#define JUNK_H
//WORKING VERSION
/**
 * @brief roughVolatility_Initialisation It initializes the rough volatility G*Y
 * @param GY Rough volatility G*Y where Y is the volatility and G the kernel
 * @param c_i parameter
 * @param gamma_i parameter
 * @param n_ld size of the arrays c_i and gamma_i.
 * It is the size of the discretization of the Laplace Integral.
 */
//void roughVolatility_Initialisation (rg_v3d * GY, double *c_i, double * gamma_i, int n_ld)
//{
//    auto start_RV = high_resolution_clock::now();

//    //Multifactor
//    p1d rv_zeta;//
//    rv_zeta [-1] = rv_zeta [1] = 0.0;
//    v2d * Uk;
//    v2d * Uk_1 = new v2d (n_ld, rv_zeta);
//    //Multifactor

//    for (int time = 0 ; time < n; time++)
//    {
//        (*GY)[time] = v2d(time+1, rv_zeta);//time+1 nodes per time
//    }
//    //Rough Volatility

//    //The goal of these loops is
//    //to initialize the rough volatility GY
//    //By Multifactor construction
//    double gk, _sum, Y_tk;
//    for (int t = 0; t < n; t++)//t = time
//    {
//        //uk new since we have deleted
//        //Uk_1 = Uk;
//        Uk = new v2d (n_ld, rv_zeta);
//        for (int k = 0; k < t; k++)//k = node/space
//        {
//            Y_tk = RV_Zeta (y_0, t, k);
//            gk = g(t*k);
//            _sum = gk;
//            for (int ud = -1; ud < 2; ud +=2)//ud==Up=+1/Down=-1
//            {
//                for (int i = 0; i < n_ld; i++)//i = Laplace discretization
//                {
//                    (*Uk) [i][ud] = 1.0/(1.0+gamma_i[i]) * (Y_tk - (*Uk_1) [i][ud]);
//                    _sum += c_i[i] * (*Uk) [i][ud];
//                }
//                (*GY) [t][k][ud] = _sum;
//            }
//        }
//        //delete forward
//        delete Uk_1;
//        Uk_1 = Uk;
//    }
//    delete Uk;
//    auto stop_RV = high_resolution_clock::now();
//    auto duration_RV = duration_cast<seconds>(stop_RV - start_RV);
//    cout << "Rough Volatility Init: " << duration_RV.count() << " seconds" << endl;
//    //delete c_i;
//    //delete gamma_i;
//}


//V2
/**
 * @brief roughVolatility_Initialisation It initializes the rough volatility G*Y.
 * It is a forward initilization. GY_k return GxY_{k+1} so GxY is predictable, and,
 * GY[k][l][-/+1] means at node (k,l) with k=time, l=space, we move up (+1) or down (-1),
 * So :
 * GY[k][l][-/+1] gives the value of GxY_{k+1}(l-1) if we move up +1, (Draw a binary tree to see l-1),
 * GY[k][l][-/+1] gives the value of GxY_{k+1}(l) if we move down -1.
 * It explain the predictability, also the forward construction, and, why we initialize with n+1 rather than n (as the other arrays)
 * GxY is initialized by a Multifactor construction.
 * @param GY Rough volatility G*Y where Y is the volatility and G the kernel
 * @param c_i parameter
 * @param gamma_i parameter
 * @param n_ld size of the arrays c_i and gamma_i.
 * It is the size of the discretization of the Laplace Integral.
 */
//rg_v3d * roughVolatility_Initialisation (double *c_i, double * gamma_i,
//                                         int n, int n_ld)
//{
//    auto start_RV = high_resolution_clock::now();
//    //Multifactor
//    p1d rv_zeta;//
//    rv_zeta [-1] = rv_zeta [1] = 0.0;
//    v2d * Uk;
//    v2d * Uk_1 = new v2d (n_ld, rv_zeta);
//    //Multifactor

//    //Rough Volatility
//    // /!\ rg_v3d(n+1) rather than n because it is predictable /!\
//    //v3d(n+1, v2d (number of nodes leq n+1))
//    rg_v3d * GY = new rg_v3d(n+1);//RV[time][Node][Previous Rv zeta = +/- 1]
//    for (int time = 0 ; time < n+1; time++)
//    {
//        (*GY)[time] = v2d(time+1, rv_zeta);//time+1 nodes per time
//    }
//    //Rough Volatility

//    //The goal of these loops is
//    //to initialize the rough volatility GY
//    //By a Multifactor construction
//    double gk, _sum, Y_tl;
//    for (int time = 0; time < n+1; time++)//t = time
//    {
//        //uk new since we have deleted it in the last instruction
//        //or never instanciated if it is the first passage
//        Uk = new v2d (n_ld);
//        //we finish l==time because Y(time+1)
//        for (int l = 0; l < time+1; l++)//l = node/space
//        {
//            //Forward construction
//            //So here we consider zeta_{k} rather than zeta_{k-1}
//            //So : UD == next move of zeta will be Up=+1/Down=-1
//            for (int UD = -1; UD < 2; UD +=2)
//            {
//                if (UD == +1)
//                    Y_tl = RV_Zeta (y_0, time+1.0, l-1.0);
//                else
//                    Y_tl = RV_Zeta (y_0, time+1.0, l);
//                gk = g((time+1.0)*l);
//                _sum = gk;
//                for (int i = 0; i < n_ld; i++)//i = Laplace discretization
//                {
//                    (*Uk) [i][UD] = (1.0/(1.0+gamma_i[i])) * (Y_tl - (*Uk_1) [i][UD]);
//                    _sum += c_i[i] * (*Uk) [i][UD];
//                }
//                //In math term
//                //(*GY) [time][l][+1] == GxY_{time+1}(l-1) which represents the node (time+1,l-1)
//                //(*GY) [time][l][-1] == GxY_{time+1}(l) which represents the node (time+1,l)
//                (*GY) [time][l][UD] = _sum;
//            }
//        }
//        //delete forward
//        delete Uk_1;
//        Uk_1 = Uk;
//    }
//    delete Uk;
//    auto stop_RV = high_resolution_clock::now();
//    auto duration_RV = duration_cast<seconds>(stop_RV - start_RV);
//    cout << "Rough Volatility Init: " << duration_RV.count() << " seconds" << endl;
//    //delete c_i;
//    //delete gamma_i;

//    return GY;
//}


//double roughSimulation(double K, double r, double eta,
//                       double kappa, double theta,
//                       double rho, double T, double nu_0,
//                       double S_0)
//{
//    double h = T/((double) n);
//    x_0 = std::log(S_0);
//    v_0 = nu_0;//nu_0/eta - rho*x_0;
//    y_0 = nu_0;//To define !!!
//    double Price = 0.0;
//    double tmp_Value = 0.0;
//    double _xi_ = -1.0;//=+/-1.0
//    double _zeta_ = -1.0;//=+/-1.0
//    double D;//Discount Factor

//    //Final Value Vector
//    p1d mxi_y;
//    mxi_y[-1] = mxi_y[1] = 0.0;
//    p2d mxi_x;
//    mxi_x[-1] = mxi_x[1] = mxi_y;

//    v4d * vk = new v4d (n, v3d(n, mxi_x));//From 0 to N-1 i.e. #[0, N-1] = N, Because l,m in [0,N-1] = N elements
//    v4d * vk_1;
//    //Final Value Vector

//    /***Discrete Kernel initialization***/
//    double c_i [n_ld];
//    double gamma_i [n_ld];
//    DKM (c_i, gamma_i, n_ld);
//    /***Discrete Kernel initialization***/

//    /***Rough Volatility Initialization***/
//    rg_v2d * GY = roughVolatility_Initialisation (c_i, gamma_i, n, n_ld);
//    /***Rough Volatility Initialization***/

//    /***Vn computation***/
//    //Initialization of the final points
//    //l,m in [0,N]
//    D = exp (-r*n*h);
//    for (int l=0; l<n; l++)//Spot
//    {
//        double Xi_n = RV_Xi(x_0,l,n);
//        for (int m=0; m<n; m++)//Vol
//        {
//            //UD_z == Zeta_n Up=+1/Down=-1
//            for (int UD_z = -1; UD_z < 2; UD_z +=2)
//            {
//                //We need the point
//                //Let zeta_{n}=+/-1
//                //GxY_{n-1}(m) if zeta_{n} = -1 we move
//                double GYn_1 = (*GY)[n-1][m][UD_z];
//                double alpha_n = alpha(GYn_1);//alpha_n=f(G*Y_[n-1])
//                p2d::iterator it_xi_x;
//                for (it_xi_x = (*vk)[l][m].begin(); it_xi_x != (*vk)[l][m].end(); ++it_xi_x)
//                {
//                    _xi_ = it_xi_x->first;
//                    tmp_Value = /*D**/ std::max(K - exp(Xi_n + sqrt_delta*alpha_n*(_xi_)), 0.0);//hat chi
//                    (*vk)[l][m][_xi_][UD_z] = tmp_Value;
//                }
//            }
//        }
//    }
//    //cout << "print number = " << N << endl;
//    //printV4D(vk);
//    /***Vn computation***/

//    //Then we execute the algorithm
//    auto start = high_resolution_clock::now();
//    double tmp_payoff, tmp_Epsilon;
//    double alpha_k_1, X_lk, X_lk_1;
//    double Y_lk_1, Y_lk;
//    double t_k, g_k;
//    for(int k_1 = n-2; k_1 >= 0; k_1--)//time
//    {
//        //        if (k_1%50==0)
//        //            cout << "time = " << k_1 << endl;
//        D = exp (-r*double(k_1)*h);
//        t_k = double((k_1+1.0)*n);
//        g_k = g (t_k);

//        //we re-initialize vk_1
//        vk_1 = new v4d (k_1+1, v3d(k_1+1, mxi_x));
//        //concurrency::parallel_for (0, k_1+1, [&](size_t l1)
//        for(int l1 = 0; l1 < k_1+1; l1++)//S=price//k_1+1=k
//        {
//            X_lk = RV_Xi(x_0, l1, k_1);
//            for(int l2 = 0; l2 < k_1+1; l2++)//Volatility
//            {
//                Y_lk = RV_Zeta (y_0, l2, k_1);
//                for (int ud = -1; ud < 2; ud += 2)//ud=Up/Down
//                {
//                    double GY_2 = (*GY) [k_1-1][l2][ud] - sqrt_delta*ud;//FAUX
//                    alpha_k_1 = alpha ((*GY) [k_1][l2][ud]);//alpha_k-1=f(V_[k-2])
//                    p2d::iterator it_xi;
//                    for (it_xi = (*vk)[l1][l2].begin(); it_xi != (*vk)[l1][l2].end(); ++it_xi)
//                    {
//                        _xi_ = it_xi->first;
//                        X_lk_1 = X_lk - sqrt_delta*_xi_;
//                        tmp_payoff = /*D **/ max (K - exp(X_lk_1+sqrt_delta*_xi_*alpha_k_1), 0.0);
//                        p1d::iterator it_zeta;
//                        for (it_zeta = (*vk)[l1][l2][_xi_].begin(); it_zeta != (*vk)[l1][l2][_xi_].end(); ++it_zeta)
//                        {
//                            _zeta_ = it_zeta->first;
//                            Y_lk_1 = Y_lk - sqrt_delta*_zeta_;
//                            tmp_Epsilon = Epsilon (&(*vk)[l1][l2],
//                                                   X_lk_1, X_lk,
//                                                   (*GY)[k_1][l2][_zeta_], (*GY)[k_1+1][l2][_zeta_],//we convey just one value
//                                    Y_lk_1, Y_lk,
//                                    _xi_, _zeta_, g_k);
//                            (*vk_1)[l1][l2][_xi_][_zeta_] = max(tmp_payoff, tmp_Epsilon);
//                        }
//                    }
//                }
//            }
//        }
//        //);
//        //for saving memory
//        delete vk;
//        vk = vk_1;//adress copy
//    }
//    auto stop = high_resolution_clock::now();
//    auto duration = duration_cast<seconds>(stop - start);
//    cout << "Time taken by Algorithm: " <<
//            duration.count() << " seconds" << endl;


//    Price = 0.25*((*vk) [0][0][-1][-1] + (*vk) [0][0][-1][1])
//            + 0.25*((*vk) [0][0][1][-1] + (*vk) [0][0][1][1]);

//    delete GY;
//    delete vk;

//    return Price;
//}


//26112020 10:53
//double roughSimulation(double K, double r, double eta,
//                       double kappa, double theta,
//                       double rho, double T, double nu_0,
//                       double S_0)
//{
//    double h = T/((double) n);
//    x_0 = std::log(S_0);
//    v_0 = nu_0;//nu_0/eta - rho*x_0;
//    y_0 = nu_0;//To define !!!
//    double Price = 0.0;
//    double tmp_payoff_n = 0.0;
//    //    double _xi_k_1 = -1.0;//=+/-1.0
//    //    double _zeta_k_1 = -1.0;//=+/-1.0

//    //    double _xi_n = -1.0;//=+/-1.0


//    //Final Value Vector
//    p1d mxi_y;
//    mxi_y[-1] = mxi_y[1] = 0.0;
//    p2d mxi_x;
//    mxi_x[-1] = mxi_x[1] = mxi_y;


//    v4d * vk = new v4d (n+1, v3d(n+1, mxi_x));//From 0 to N i.e. #[0, N] = N, Because l,m in [0,N] = N+1 elements
//    v4d * vk_1;
//    v4d * vn = new v4d (n+1, v3d(n+1, mxi_x));
//    //Final Value Vector

//    /***Discrete Kernel initialization***/
//    double c_i [n_ld];
//    double gamma_i [n_ld];
//    DKM (c_i, gamma_i, n_ld);
//    /***Discrete Kernel initialization***/

//    /***Rough Volatility Initialization***/
//    rg_v2d * GY = roughVolatility_Initialisation (c_i, gamma_i, n+1, n_ld);
//    /***Rough Volatility Initialization***/

//    /***Vn computation***/
//    //Initialization of the final points
//    //l,m in [0,N]
//    double alpha_n, GYn_1, Sn, hat_Xn;
//    double D_n = exp (-r*T);
//    //We initiate the values at time n
//    for (int l=0; l<=n; l++)//Spot
//    {
//        double Xi_n = RV_Xi(x_0,l,n);
//        for (int m=0; m<=n; m++)//Vol
//        {
//            if (m <n)
//                GYn_1 = (*GY)[n-1][m];
//            else
//                GYn_1 = (*GY)[n-1][m-1];
//            alpha_n = alpha(n-1, GYn_1);//alpha_n=f(G*Y_[n-1])

//            for (int _xi_n = -1; _xi_n < 2; _xi_n +=2)
//            {

//                //double tmp_discount = exp(-r*(n)*delta);
//                hat_Xn = Xi_n + sqrt_delta*alpha_n*_xi_n;
//                Sn = exp(hat_Xn);
//                tmp_payoff_n = std::max(D_n*(K - Sn), 0.0);//hat xi
//                (*vn)[l][m][_xi_n][+1] = (*vn)[l][m][_xi_n][-1] = tmp_payoff_n;
//            }

//        }
//    }
//    /***Vn computation***/

//    //Then we execute the algorithm
//    auto start = high_resolution_clock::now();

//    double tmp_payoff_k_1, tmp_Epsilon_k;
//    double alpha_k_1, X_lk_1, X_lk_2;
//    double Y_lk_2, Y_lk_1;
//    double Dk_1;
//    double Sk_1;
//    //    double Discount = exp(-r*delta);//Discount Factor
//    //double gk_1;
//    double GYk_2;
//    p2d * tmp_value_k = new p2d (mxi_x);

//    vk = vn;
//    //we pursue at time n-1
//    //#pragma omp parallel for
//    double sum_init_new_vk = 0.0;
//    for(int tk_1 = n-1; tk_1 >= 0; tk_1--)//time : n-2 means n-1
//    {
//        if (tk_1%50==0)
//            cout << "time = " << tk_1 << endl;
//        Dk_1 = exp ( -r*((double) tk_1)*delta );

//        //we re-initialize vk_1
//        clock_t start_vk = clock();
//        vk_1 = new v4d (tk_1+1, v3d(tk_1+1, mxi_x));
//        clock_t stop_vk = clock();
//        sum_init_new_vk += (double) (stop_vk - start_vk)/CLOCKS_PER_SEC;

//        for(int l1 = 0; l1 <= tk_1; l1++)//S=price//k_1+1=k
//        {
//            X_lk_1 = RV_Xi(x_0, l1, tk_1);//tk_1 ??? not tk_1-1 ???
//            for(int l2 = 0; l2 <= tk_1; l2++)//Volatility
//            {
//                Y_lk_1 = RV_Zeta (y_0, l2, tk_1);//tk_1-1 because 0<=l,m<=k-1

//                if (tk_1 > 0)
//                {
//                    if (l2 < tk_1)//on the edge
//                        GYk_2 = (*GY) [tk_1-1][l2];
//                    else
//                        GYk_2 = (*GY) [tk_1-1][l2-1];
//                    alpha_k_1 = alpha (tk_1-1.0, GYk_2);//alpha_{k-1}=f(V_[k-2])
//                }
//                else if (tk_1 == 0)//we are at time t0
//                {
//                    GYk_2 = v_0;
//                    alpha_k_1 = 0.0;
//                }
//                (*tmp_value_k)[+1][+1] = (*vk) [l1][l2][+1][+1];
//                (*tmp_value_k)[-1][+1] = (*vk) [l1+1][l2][-1][+1];
//                (*tmp_value_k)[+1][-1] = (*vk) [l1][l2+1][+1][-1];
//                (*tmp_value_k)[-1][-1] = (*vk) [l1+1][l2+1][-1][-1];

//                for (int _xi_k_1 = -1; _xi_k_1 < 2; _xi_k_1 +=2)
//                {
//                    X_lk_2 = X_lk_1 - sqrt_delta*_xi_k_1;
//                    for (int _zeta_k_1 = -1; _zeta_k_1 < 2; _zeta_k_1 +=2)
//                    {
//                        Y_lk_2 = Y_lk_1 - sqrt_delta*_zeta_k_1;
//                        tmp_Epsilon_k = Dk_1 * Epsilon (&(*tmp_value_k), tk_1,
//                                                        X_lk_2, X_lk_1,
//                                                        GYk_2, (*GY)[tk_1][l2],//we convey just one value
//                                                        Y_lk_2, Y_lk_1,
//                                                        _xi_k_1, _zeta_k_1);
//                        Sk_1 = exp(X_lk_1+sqrt_delta*alpha_k_1*_xi_k_1);
//                        tmp_payoff_k_1 = max (Dk_1 * (K - Sk_1), 0.0);
//                        (*vk_1)[l1][l2][_xi_k_1][_zeta_k_1] = max(tmp_payoff_k_1, tmp_Epsilon_k);
//                    }
//                }
//            }
//        }
//        //for saving memory
//        delete vk;
//        vk = vk_1;//adress copy
//    }
//    auto stop = high_resolution_clock::now();
//    auto duration = duration_cast<seconds>(stop - start);
//    cout << "Time taken by Algorithm: " <<
//            duration.count() << " seconds" << endl;
//    cout << "Time taken by new vk : " <<
//            sum_init_new_vk << " seconds" << endl;

//    Price = 0.25*((*vk) [0][0][-1][-1] + (*vk) [0][0][-1][1])
//            + 0.25*((*vk) [0][0][1][-1] + (*vk) [0][0][1][1]);

//    delete GY;
//    delete vk;
//    //delete vn;
//    delete tmp_value_k;

//    return Price;
//}






#endif // JUNK_H
